<?php

/* CatalogFrontendBundle:Category:subcategories.html.twig */
class __TwigTemplate_7811394ef02dc266550d1322a96719a17da0f79214614ee55ca271c2b76888a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"site-navigation\">
<a class=\"categories-link\" href=\"#\" path=\"";
        // line 2
        echo $this->env->getExtension('routing')->getPath("ajax_catalog_categories");
        echo "\">Категории</a> > ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "category"), "name"), "html", null, true);
        echo "
</div>
";
        // line 4
        $this->displayBlock('body', $context, $blocks);
    }

    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        // line 6
        echo "    <div class=\"categories-list\">
        ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "subcategories"));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 8
            echo "                <div class=\"category\">
                    <div class=\"category-image\">
                        <img src=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "category"), "imagePath"), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"category-information\">
                        <div class=\"category-name\">
                            <a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("catalog_subcategories", array("categoryId" => $this->getAttribute($this->getContext($context, "category"), "id"))), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "category"), "name"), "html", null, true);
            echo "</a>
                        </div>
                    </div>
                </div> 
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "CatalogFrontendBundle:Category:subcategories.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 19,  56 => 14,  49 => 10,  45 => 8,  41 => 7,  38 => 6,  36 => 5,  30 => 4,  23 => 2,  20 => 1,);
    }
}
