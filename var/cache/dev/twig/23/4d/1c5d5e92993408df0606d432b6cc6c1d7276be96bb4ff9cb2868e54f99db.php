<?php

/* CatalogFrontendBundle:Category:ajax_load_categories.html.twig */
class __TwigTemplate_234d1c5d5e92993408df0606d432b6cc6c1d7276be96bb4ff9cb2868e54f99db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"site-navigation\">
    Категории
</div>
";
        // line 4
        $this->displayBlock('body', $context, $blocks);
    }

    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        // line 6
        echo "    <div class=\"categories-list\">
        ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "categories"));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 8
            echo "                <div class=\"category\">
                    <div class=\"category-image\">
                        <img src=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "category"), "imagePath"), "html", null, true);
            echo "\"/>
                    </div>
                    <div class=\"category-information\">
                        <div class=\"category-name\">
                            <a href=\"#\" path=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("catalog_subcategories", array("categoryId" => $this->getAttribute($this->getContext($context, "category"), "id"))), "html", null, true);
            echo "\" class='subcategories-load'>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "category"), "name"), "html", null, true);
            echo "</a>
                        </div>
                        <ul class=\"subcategories\">
                            ";
            // line 18
            echo "                            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "category"), "subcategory"));
            foreach ($context['_seq'] as $context["_key"] => $context["subcategory"]) {
                // line 19
                echo "                                <li class=\"subcategory\">
                                    <a href=\"#\">";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "subcategory"), "name"), "html", null, true);
                echo "</a>
                                </li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subcategory'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "                        </ul>
                    </div>
                </div> 
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "CatalogFrontendBundle:Category:ajax_load_categories.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 27,  76 => 23,  67 => 20,  64 => 19,  59 => 18,  51 => 14,  44 => 10,  40 => 8,  36 => 7,  33 => 6,  31 => 5,  25 => 4,  20 => 1,);
    }
}
