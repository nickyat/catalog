/**
 *
 * @param short_name
 * @param full_name
 * @param description
 */
function getPriceInfo(short_name, full_name) {

    $.ajax({
        url: 'http://sb.avantkursk.ru/price?getInfo',
        //url: '/app_dev.php/ajax/test',
        type: 'POST',
        data: {
            "short_name": short_name,
            "full_name": full_name
        },
        success: function(data) {
            var response = jQuery.parseJSON(data);
            $("#basket").append(response.sData);

        },
        error: function(e) {
            return e;
        }
    });
}

 var filters = [];
 var brand;
function initialise(items_count, per_page, page)
{
        $('#pagination').pagination({
            items: $('.goods-list').attr('count'),
            itemsOnPage: per_page,
            currentPage: page,
            cssStyle: 'light-theme',
            onPageClick: function(pageNumber, event){
                loadGoods($('.goods-count').attr('path'), pageNumber, $('.goods-count span.active').text(), $('.sorting .active').attr('order'));
            }
        });
}


function loadGoods(path, page, per_page, order)
{
    $.post(path,        
        { 
            page: page,
            order: order,
            per_page: per_page,
            filters: filters,
            brand: brand
        }, 
        function(response){
            $('html').empty();
            $('html').append(response);
            
            initialise(100, $('.goods-count span.active').text(), page);
            pageurl = path;
            if(pageurl!=window.location){
                 window.history.pushState({path:pageurl},'',pageurl);
            }
            return false;
            
        });
}

function getCurrentPage()
{
    $('#pagination .active span').each(function(){
        if ( $(this).html() !== 'Next' && $(this).html() !== 'Prev'){
            return $(this).html();
        }
    });
    
}

function saveFilters()
{
   filters = [];
    $('.attribute-filter').each(function(){
        item = {};
        item[$(this).attr('name')] = $(this).val();
        filters.push(item);
    });
    brand = $('.brand').val();
}

$(document).ready(function(){
    initialise(100, $('.goods-count span.active').text(), 1);
   // Вешаем событие click на ссылку, по нажатии которой будет происходить ajax подгрузка субкатегорий
   $(document).on('click', '.subcategories-load', function(){
       $.post($(this).attr('path'),        
        { 
            
        }, 
        function(response){
            $('html').empty();
            $('html').append(response);
            pageurl = $(this).attr('href');
            if(pageurl!=window.location){
                 window.history.pushState({path:pageurl},'',pageurl);
            }
            return false;
        });
   });
   
   // Ajax подгрузка всех категорий
   $(document).on('click','.categories-link', function(){
       $.post($(this).attr('path'),        
        { 
            
        }, 
        function(response){
            $('html').empty();
            $('html').append(response);
            pageurl = $(this).attr('href');
            if(pageurl!=window.location){
                 window.history.pushState({path:pageurl},'',pageurl);
            }
            return false;
        });
   });
   
   // Загрузка товаров
   $(document).on('click','.goods-load', function(){
       loadGoods($(this).attr('href'),  getCurrentPage(),  $('.goods-count span.active').text(),  $('.sorting.active').val());
       
       
   });
   
   // Карточка товара
   $(document).on('click','.item-load', function(){
       pageurl = $(this).attr('path');
       $.post(pageurl,        
        { 
            
        }, 
        function(response){
            $('html').empty();
            $('html').append(response);
            
            if(pageurl!=window.location){
                 window.history.pushState({path:pageurl},'',pageurl);
            }
            return false;
        });
   });
   
   $(document).on('click','.finded-item', function(){
       pageurl = $(this).attr('path');
       $.post($(this).attr('path'),        
        { 
            
        }, 
        function(response){
           $('html').empty();
            $('html').append(response);
            if(pageurl!=window.location){
                 window.history.pushState({path:pageurl},'',pageurl);
            }
            return false;
        });
   });
   
   // Загрузка товаров
   $(document).on('click', '.sorting', function(){
       saveFilters();
       loadGoods($(this).attr('path'), getCurrentPage() ,  $('.goods-count span.active').text(),  $(this).attr('order'));
   });
   
   $(document).on('click', '.goods-count span', function(){
       saveFilters();
       loadGoods($(this).closest('.goods-count').attr('path'),  getCurrentPage(),  $(this).text(),  $('.sorting.active').val());
   });
   
   

   $('#search-string').donetyping(function(){
       $.post($(this).attr('path'),
           {
               search_string: $(this).val()
           },
           function(response){
               if (response){
                   $('.search-result').empty();
                   $('.search-result').css('display', 'block');
                   $('.search-result').append(response);
               }else{
                   $('.search-result').empty();
                   $('.search-result').css('display', 'none');
               }
           });
   });

    $(document).on("click", ".content", function(){
        $('.search-result').fadeOut();
    });

   $(document).on('focusout', '#search-string', function(){
       $('.search-result').css('dispay', 'none');
   });

   $(document).on("click", "#choosen-brand", function(){
       var category = $(this).parent().attr("category");
       var path = $(this).parent().attr("path");
       var brand = $(this).attr("brand");
       var element = $(this);
       $.ajax({
           //url: 'http://sb.avantkursk.ru/price?getInfo',
           url: path,
           type: 'POST',
           data: {
               brand: brand,
               category: category
           },
           success: function(data) {
               element.fadeOut();
               window.location.reload();
               //$('.brand').val("all").trigger("change");
           },
           error: function(e) {
               console.log(e);
           }
       });

   });

    $(document).on('click', '#remove-filters', function(){
        $.ajax({
            url: $(this).attr('path'),
            type: 'POST',
            data: {
                category: $(this).attr('category'),
                session: $(this).attr('session')
            },
            success: function(response)
            {
                window.location.reload();
            },
            error: function(error)
            {

            }
        });
    })

   $(document).on('change', '.filter-container select', function(){

      saveFilters();

       $.ajax({
           //url: 'http://sb.avantkursk.ru/price?getInfo',
           url: $(this).closest('.filter-container').attr('path'),
           type: 'POST',
           data: {
               filters: filters,
               brand: brand
           },
           beforeSend: function(){
               $(".goods-list").addClass("site-hover");
           },
           success: function(response) {

               $('html').empty();
               $('html').append(response);
               $(".goods-list").removeClass("site-hover");
               window.location.reload();
           },
           error: function(e) {
               console.log(e);
           }
       });
   });

   

   $(document).on('click', '.settings-button', function(){
       $(this).toggleClass('active');
       if ($('.filter-attributes-container').is(':visible')){
           $('.filter-attributes-container').fadeOut(); 
       }else{
           $.post($(this).attr('path'),
             { 
             }, 
             function(response){
                $('.filter-attributes-container').fadeIn();
                $('.filter-attributes-container').empty();
                $('.filter-attributes-container').append(response);
                
            });
      }
        
   });
   
   $(document).on('click', '.save-filter', function(){
      var filtersSettings = [];
      $('.filter-value').each(function(){
          item = {};
          item[$(this).attr('name')] = (this.checked ? 1 : 0 );;
          filtersSettings.push(item);
      });
      var filters = [];
      $('.attribute-filter').each(function(){
          item = {};
          item[$(this).attr('name')] = $(this).val();
          filters.push(item);
      });
      $.post($(this).closest('.filter-container').attr('path'),
        { 
            filters: filters,
            filters_settings: filtersSettings
            
        }, 
        function(response){
            $('html').empty();
            $('html').append(response);
        });
      
   });
   $(document).on('click', '.reset-filter', function(){
       $.post($(this).attr('path'),
        { 
        }, 
        function(response){
            $('.content').empty();
            $('.content').append(response);
        });
   });


});




;(function($){
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress',function(e){
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too premptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type=='keyup' && e.keyCode!=8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);