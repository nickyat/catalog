<?php

namespace Catalog\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class GoodsAdmin extends Admin
{
        protected $datagridValues = array(
            '_sort_order' => 'DESC',
            '_per_page' => 2,
        );
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('Основное')
            ->add('subcategory', 'entity', array( 
                'label' => 'Имя категории', 
                'required' => true, 
                'class' => 'CatalogBackendBundle:Category', 
                'multiple' => false,
                'property' => 'name',
                'attr' => array(
                    'class' => 'subcategory')))
            ->add('brand', 'text', array('label' => 'Бренд'))
            ->add('article', 'text', array('label' => 'Артикль'))
            ->add('rating',   'integer', array('label' => 'Рейтинг'))
            ->add('full_name', 'text', array('label' => 'Полное наименование'))
            ->add('description', 'textarea', array('label' => 'Описание', 'attr' => array('class' => 'ckeditor')))

        ;
        
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('subcategory', null, array('label' => 'Подкатегория'))
            ->add('brand', null, array('label' => 'Бренд'))
            ->add('article', null, array('label' => 'Артикль'))
            ->add('rating', null, array('label' => 'Оценка'))
            ->add('full_name', null, array('label' => 'Полное имя'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('subcategory', 'entity', array(
                'class' => 'Catalog/Backend/Entity/Subcategory.php',
                'associated_property' => 'name',
                'label' => 'Подкатегория'))
            ->addIdentifier('brand', 'string', array('label' => 'Бренд'))
            ->addIdentifier('article', null, array('label' => 'Артикль'))
            ->addIdentifier('rating', 'integer', array('label' => 'Оценка'))
            ->addIdentifier('full_name', null, array('label' => 'Полное имя'))
            ->addIdentifier('image_preview', null, array('label' => 'Первое изображение', 'template' => 'CatalogFrontendBundle:Admin:goods_image.html.twig'))
            ->add('_action', 'actions', array(
            'actions' => array(
                'Clone' => array(
                    'template' => 'CatalogAdminBundle:CRUD:list__action_clone.html.twig'
                    )
                )
            ))
        ;
    
        
    }
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('clone', $this->getRouterIdParameter().'/clone');
    }
    
    /*public function prePersist($image) {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image) {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image) {
        var_dump();
    }*/
}