<?php

namespace Catalog\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SubcategoryAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'Subcategory name'))
            ->add('category', 'entity', array('required' => true, 'class' => 'CatalogBackendBundle:Category', 'multiple' => false, 'property' => 'name'))
            ->add('file', 'file', array('label' => 'Subcategory image', 'required' => false, 'data_class' => null))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('category.name')
            ->add('image_preview', null, array('label' => 'Image', 'template' => 'CatalogFrontendBundle:Admin:category_image.html.twig'))
            
        ;
    }
    
    public function prePersist($image) {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image) {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image) {
        if ($image->upload()) {
        }
    }
}