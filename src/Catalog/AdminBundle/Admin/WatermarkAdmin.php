<?php

namespace Catalog\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class WatermarkAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
        
    }
    
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->add('file', 'file', array('label' => 'Изображение', 'required' => true, 'data_class' => null))
        ->add('position', 'choice', array(
                'label' => 'Расположение водяного знака',
                'choices' => array('left-top' => 'Слева сверху', 'rigth-top' => 'Справа сверху', 'left-bottom' => 'Слева снизу', 'right-bottom' => 'Справа снизу', 'center' => 'По центру'
                    )))        
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('position', null, array('label' => "Позиция"))
            ->add('image_preview', null, array('label' => 'Изображение', 'template' => 'CatalogFrontendBundle:Admin:watermark_image.html.twig'))
            
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('position')
        ;
    }
    
    public function prePersist($image) {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image) {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image) {
        if ($image->upload()) {
        }
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

