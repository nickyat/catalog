<?php

namespace Catalog\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class GoodsPhotoAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('position', 'choice', array(
                'label' => 'Номер позиции',
                'choices' => array(1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5'
                    )))
            
        ;
        if($this->getSubject()){
            $formMapper->add('file', 'file', array('label' => 'Изображение товара', 'required' => false,'help' => '<img style="width: 200px; height:200px;" src="'.$this->getSubject()->getPath().'" class="photo-preview" />'));
        }else{
            $formMapper->add('file', 'file', array('label' => 'Изображение товара', 'required' => false));
        }
        
    }


    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')        ;
    }
    
//    public function prePersist($image) {
//        $this->manageFileUpload($image);
//    }
//
//    public function preUpdate($image) {
//        $this->manageFileUpload($image);
//    }
//
//    private function manageFileUpload($image) {
//        if ($image->getFile()) {
//            
//        }
//    }
}