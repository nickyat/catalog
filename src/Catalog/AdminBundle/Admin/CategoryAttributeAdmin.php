<?php

namespace Catalog\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CategoryAttributeAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('orders', 'integer', array('label' => 'Позиция'))
            ->add('groups', 'text', array('label' => 'Группа'))
            ->add('name', 'text', array('label' => 'Имя'))
            ->add('validator', 'choice', array('label' => 'Валидатор','choices' => array('none' => "Отсутствует", 'int' => "Числовой", 'range' => 'Диапазон')))
            ->add('validator_string', 'text', array('label' => 'Cтрока валидации (при валидаторе "Диапазон")'))
            ->add('show_in_list', 'choice', array('label' => 'Отображение в витрине','choices' => array(true => "Да", false => "Нет")))
            ->add('show_in_filter', 'choice', array('label' => 'Отображение в фильтре','choices' => array(true => "Да", false => "Нет")));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
        ;
    }
}