<?php

namespace Catalog\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CategoryAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'Имя категории'))
            ->end()
            ->with('Аттрибуты')
                ->add('attribute', 'sonata_type_collection', array(
                    'required' => false,
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable'  => 'position'
            ))
            ->add('parent', 'entity', array('label' => 'Родительская категория (опционально)','required' => false, 'class' => 'CatalogBackendBundle:Category', 'multiple' => false, 'property' => 'name'))
            ->add('file', 'file', array('label' => 'Изображение категории', 'required' => false))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('parent.name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name', null, array('label' => 'Имя'))
            ->addIdentifier('parent.name', 'entity', array(
                'class' => 'Catalog/Backend/Entity/SubCategory.php',
                'associated_property' => 'parent',
                'label' => 'Имя родителькой категории'))
            ->add('image_preview', null, array('label' => 'Изображение', 'template' => 'CatalogFrontendBundle:Admin:category_image.html.twig'))
            
        ;
    }
    
    public function prePersist($image) {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image) {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image) {
        if ($image->upload()) {
        }
    }
}