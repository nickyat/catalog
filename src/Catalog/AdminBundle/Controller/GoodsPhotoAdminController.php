<?php
namespace Catalog\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Catalog\BackendBundle\Entity\GoodsPhoto;
use Catalog\BackendBundle\Entity\GoodsAttribute;
use Symfony\Component\Finder\Finder;
use Catalog\BackendBundle\Entity\CategoryAttribute;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Imagine\Gd\Image;
use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class GoodsPhotoAdminController extends Controller
{
     public function createAction()
    {
        $templateKey = 'edit';
        
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        $object = $this->admin->getNewInstance();

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);
        $formName = $form->getName();
        $datas = $this->get('request')->request->all();
        $files = $this->get('request')->files->all();
        if ($this->getRestMethod()== 'POST') {
            $form->submit($this->get('request'));
            $isFormValid = $form->isValid();
            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                
                /**/
                if (false === $this->admin->isGranted('CREATE', $object)) {
                    throw new AccessDeniedException();
                }

                    $object = $this->admin->create($object);

                
                $entityManager = $this->getDoctrine()->getManager();
                
                    $attributes = array();
                    foreach ($datas as $key => $data){
                        if ($key != $formName && $key != 'btn_create_and_edit' && $key != 'btn_create_and_list' && $key !='btn_create_and_create' && $key != 'photos'){
                            $categoryAttribute = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->find($key);
                            $goodsAttribute = new GoodsAttribute();
                            $goodsAttribute->setCategoryAttribute($categoryAttribute);
                            $goodsAttribute->setValue($data);
                            $goodsAttribute->setGoods($object);

                            $entityManager->persist($goodsAttribute);
                        }
                    }

                    $photos = $datas['photos']['photo'];
                    if ($photos){
                        foreach ($photos as $key => $photo){
                            $file = new File($this->getUploadRootDir().$photo);
                            $newPhoto = new GoodsPhoto();
                            $newPhoto->setGoods($object);
                            $newPhoto->setPosition($datas['photos']['position'][$key]);
                            $newPhoto->setPath($this->upload($file, $object, $datas['photos']['position'][$key]));

                            $entityManager->persist($newPhoto);
                        }
                    }

                    $entityManager->flush();

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object)
                        ));
                    }

                    $this->addFlash('sonata_flash_success', $this->admin->trans('flash_create_success', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));

                    // redirect to edit mode
                    return $this->redirectTo($object);
                }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', $this->admin->trans('flash_create_error', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));
                }
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'create',
            'form'   => $view,
            'object' => $object,
        ));
    }
    
    
    public function editAction($id = null)
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);
        
        $formName = $form->getName();
        $datas = $this->get('request')->request->all();
        if ($this->getRestMethod() == 'POST') {
            $form->submit($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $object = $this->admin->update($object);
                
                
                $entityManager = $this->getDoctrine()->getManager();
            
                $attributes = array();
                foreach ($datas as $key => $data){
                    if ($key != $formName && $key != 'btn_update_and_edit' && $key != 'btn_update_and_list' && $key != 'photos'){
                        $categoryAttribute = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->find($key);
                        $goodsAttribute = $entityManager->getRepository('CatalogBackendBundle:GoodsAttribute')->findOneBy(array(
                            'category_attribute' => $categoryAttribute,
                            'goods' => $object->getId()
                        ));
                        if ($goodsAttribute){
                            $goodsAttribute->setValue($data);
                            $entityManager->persist($goodsAttribute);
                        }else{
                            $goodsAttribute = new GoodsAttribute();
                            $goodsAttribute->setCategoryAttribute($categoryAttribute);
                            $goodsAttribute->setValue($data);
                            $goodsAttribute->setGoods($object);

                            $entityManager->persist($goodsAttribute);
                        }
                    }
                }
        
                if (isset($datas['photos']['photo'])){
                    $photos = $datas['photos']['photo'];
                    if ($photos){
                        foreach ($photos as $key => $photo){
                            $ph = $object->getPhoto();
                            if (isset($ph[$key])){
                                $entityManager->remove($ph[$key]);
                            }
                            $file = new File($this->getUploadRootDir().$photo);
                            $newPhoto = new GoodsPhoto();
                            $newPhoto->setGoods($object);
                            $newPhoto->setPosition($datas['photos']['position'][$key]);
                            $newPhoto->setPath($this->upload($file,$object, $datas['photos']['position'][$key]));

                            $entityManager->persist($newPhoto);
                        }
                        
                    }
                    
                }
                $entityManager->flush();
                if (isset($datas['photos']['position'])){
                    $key = 0;
                    $photos = $entityManager->getRepository('CatalogBackendBundle:GoodsPhoto')->findBy(array('goods' => $object));
                    foreach ($datas['photos']['position'] as $pos){
                                $photos[$key]->setPosition($pos);
                                $entityManager->persist($photos[$key]);
                                $key++;
                    }
                }
                $entityManager->flush();
                

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result'    => 'ok',
                        'objectId'  => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                $this->addFlash('sonata_flash_success', $this->admin->trans('flash_edit_success', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', $this->admin->trans('flash_edit_error', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form'   => $view,
            'object' => $object,
        ));
    }
    
    public function cloneAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $entityManager = $this->getDoctrine()->getManager();
        $clonedObject = clone $object;  
        $this->admin->create($clonedObject);
        $images = $object->getPhoto();
        foreach ($images as $image){
            $newImage = clone $image;
            $newImage->setGoods($clonedObject);
            $entityManager->persist($newImage);
        }
        $attributes = $object->getAttribute();
        foreach ($attributes as $attribute){
            $newAttr = clone $attribute;
            $newAttr->setGoods($clonedObject);
            $entityManager->persist($newAttr);
        }
        
        $entityManager->flush();

        $this->addFlash('sonata_flash_success', 'Cloned successfully');

        return new RedirectResponse($this->admin->generateUrl('edit', array('id' => $object->getId())));
    }
    
    public function listAction()
    {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $datagrid = $this->admin->getDatagrid();
        $pager = $datagrid->getPager();
        $pager->setMaxPerPage(200);
        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render($this->admin->getTemplate('list'), array(
            'action'     => 'list',
            'form'       => $formView,
            'datagrid'   => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }
    
    
    
    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return 'http://sb.avantkursk.ru/Catalog/web/images/';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'images/';
    }
    
    public function upload($file, $object, $position)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $newFilename = md5(microtime());
        $extention = $file->guessExtension();
        $article = preg_replace('/\s+/', '', $object->getArticle());
        $newFilename = $article .'_' .$position .'.' . $extention;
        $folderNameString = preg_replace('/[^ \w]+/', '', $object->getBrand());
        $folderName = $this->getUploadRootDir() . $folderNameString .'/';
        $fs = new Filesystem();
        try {
            $fs->mkdir($folderName , 0777);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
        }
        
        $file->move(
            $folderName,
            $newFilename
        );
     
        return $this->getUploadDir() . $folderNameString . '/' .  $newFilename;
    }
}