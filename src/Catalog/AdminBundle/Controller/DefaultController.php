<?php

namespace Catalog\AdminBundle\Controller;

use Catalog\BackendBundle\Entity\CategoryAttribute;
use Catalog\BackendBundle\Entity\ItemAttribute;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Catalog\BackendBundle\Entity\Goods;
use Catalog\BackendBundle\Entity\Xml;
use Catalog\BackendBundle\Entity\GoodsPhoto;
use Catalog\BackendBundle\Entity\GoodsAttribute;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Sonata\AdminBundle\Controller\CoreController;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends CoreController
{


    public function indexAction()
    {
        $data = array();

        $form = $this->createFormBuilder($data)
            ->add('xml', 'file')
            ->add('replacement', 'checkbox', array('label' => 'Заменять существующие товары', 'required' => false))
            ->add('save', 'submit', array('label' => 'Создать товар'))
            ->getForm();

        return $this->render('CatalogFrontendBundle:Goods:xml_import.html.twig',
            array(
                'form'          => $form->createView(),

                'base_template' => $this->getBaseTemplate(),
                'admin_pool'    => $this->container->get('sonata.admin.pool'),
                'blocks'        => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'))
        );
    }

    /**
     * @param $file
     * @return bool
     */
    private function isXmlStructureValid($file)
    {
        $prev = libxml_use_internal_errors(true);
        $ret = true;
        try {
            new \SimpleXMLElement($file, 0, true);
        } catch (Exception $e) {
            $ret = false;
        }
        if (count(libxml_get_errors()) > 0) {
            // There has been XML errors
            $ret = false;
        }
        // Tidy up.
        libxml_clear_errors();
        libxml_use_internal_errors($prev);

        return $ret;
    }

    public function xmlFileuploadAction(Request $request)
    {
        $data = json_decode($request->getContent());
        $downloadPath = $this->get('kernel')->getRootDir() . '/../web/images/' . $data->directory . '/';
        $response = array();
        if (file_put_contents($downloadPath . $data->filename, file_get_contents($data->link))) {
            $response['uploaded'] = true;
            $status = 200;
        } else {
            $response['uploaded'] = false;
            $status = 400;
        }

        return new JsonResponse($response, $status);
    }

    public function xmlInsertAction(Request $request)
    {

        $data = json_decode($request->getContent());

        $xmlFilename = $data->link;
        $entityManager = $this->getDoctrine()->getManager();
        $replacement = true;
//        $errors = array();
//        $stats = array(
//            "added"                => 0,
//            "replaced"             => 0,
//            "broken"               => 0,
//            "images_added"         => 0,
//            "attributes_added"     => 0,
//            "not_replaced"         => 0,
//            "attributes_not_added" => 0
//        );
        $fs = new Filesystem();


        //$downloadPath = $this->getUploadRootDir();
        $downloadPath = $this->get('kernel')->getRootDir() . '/../web/images/';


        $xmlFile = simplexml_load_file($downloadPath . $xmlFilename);

        $imagesArray = array();
        foreach ($xmlFile->item as $xml) {
            $category = $entityManager->getRepository('CatalogBackendBundle:Category')
                ->find($xml->category_id);
            if(!empty($category)) {
                if ($data->replace == true) {
                    $item = $entityManager->getRepository('CatalogBackendBundle:Goods')
                        ->findOneBy(
                            array(
                                'brand'       => $xml->brand,
                                'article'     => $xml->article,
                                'subcategory' => $xml->category_id
                            )
                        );
                }

                if ((!$item && $data->replace == true) || $data->replace == false) {
                    $item = new Goods();
                } else {
                    $this->removeOldAttrsAndPhotos($item);
                }

                $item->setSubcategory($entityManager->getReference('CatalogBackendBundle:Category', $xml->category_id));
                if ($xml->brand) {
                    $item->setBrand($xml->brand);
                }
                if ($xml->article) {
                    $item->setArticle($xml->article);
                }
                if ($xml->fullname) {
                    $item->setFullName($xml->fullname);
                }
                if ($xml->description) {
                    $item->setDescription($xml->description);
                }
                if ($xml->rating) {
                    $item->setRating((int)$xml->rating);
                }

                $entityManager->persist($item);

                $xmlImages = $xml->images;
                $itemId = $item->getId();
                if (count($xmlImages)) {
                    foreach ($xmlImages->image as $image) {
                        $goodsPhoto = new GoodsPhoto();
                        foreach ($image as $key => $rootImage) {

                            if ($key == 'position') {
                                $goodsPhoto->setPosition($rootImage);
                            }

                            if ($key == 'url') {
                                $article = $item->getArticle();
                                if (empty($article)) {
                                    $article = $item->getFullname();
                                }

                                $article = preg_replace('/\s+/', '', $article);
                                $newFilename = $article . '_' . md5(rand(0, 100)) . '.jpg';

                                $folderNameString = preg_replace('/[^ \w]+/', '', $item->getBrand());
                                $folderName = $downloadPath . $folderNameString . '/';
                                //$folderName = $downloadPath;


                                $goodsPhoto->setPath("/images/" . $folderNameString . "/" . $newFilename);

                                $entityManager->persist($goodsPhoto);
                                $goodsPhotoId = $goodsPhoto->getId();
                                $imagesArray[$goodsPhotoId]['id'] = $goodsPhotoId;
                                $imagesArray[$goodsPhotoId]['directory'] = $folderNameString;
                                $imagesArray[$goodsPhotoId]['filename'] = $newFilename;
                                $imagesArray[$goodsPhotoId]['link'] = $rootImage;

                                try {
                                    $fs->mkdir($folderName, 0777);
                                } catch (IOExceptionInterface $e) {
                                    echo "An error occurred while creating your directory at " . $e->getPath();
                                }


                            }


                            $goodsPhoto->setGoods($item);
                        }
                    }
                }


                if ($xml->attributes) {
                    foreach ($xml->attributes as $c) {
                        foreach ($c as $a => $t) {
                            $findCategory = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->findOneBy(
                                array(
                                    'category' => $item->getSubcategory(),
                                    'name'     => (string)$t['name'])
                            );


                            if ($findCategory) {

                                $valuesArray = explode('|', $t);
                                if(count($valuesArray) > 0){
                                    foreach($valuesArray as $attribute){
                                        $goodsAttributes = new GoodsAttribute();
                                        $goodsAttributes->setCategoryAttribute($findCategory);
                                        $goodsAttributes->setGoods($item);
                                        $goodsAttributes->setValue($attribute);
                                        $entityManager->persist($goodsAttributes);
                                    }
                                }else{
                                    $goodsAttributes = new GoodsAttribute();
                                    $goodsAttributes->setCategoryAttribute($findCategory);
                                    $goodsAttributes->setGoods($item);
                                    $goodsAttributes->setValue((string)$t);
                                    $entityManager->persist($goodsAttributes);

                                }

                            }
                        }
                    }
                }
            }else{
                //TODO категории не существует
            }
        }
        $entityManager->flush();
        if (count($imagesArray) <= 0) {
            $imagesArray = null;
            $status = 400;
        } else {
            $status = 200;
        }

        return new JsonResponse(json_encode($imagesArray), $status);

    }


    public
    function xmlUploadAction(Request $request)
    {
        $data = array();
        $goodsId = 0;
        $form = $this->createFormBuilder($data)
            ->add('xml', 'file')
            ->add('replacement', 'checkbox', array('label' => 'Заменять существующие товары', 'required' => false))
            ->add('save', 'submit', array('label' => 'Создать товар'))
            ->getForm();


        if ($request->isMethod('POST')) {

            $entityManager = $this->getDoctrine()->getManager();
            $filename = sha1(microtime());
            $dowloadPath = $this->get('kernel')->getRootDir() . '/../web/images/';
            $file = $request->files->get('form');
            $formData = $request->get('form');


            $file['xml']->move($dowloadPath, $filename . '.xml');

            $filepath = $dowloadPath . $filename . '.xml';
            $xmlName = $filename . '.xml';
            $xmlEntity = new Xml();
            $response = array();
            if ($this->isXmlStructureValid($filepath)) {
                $xmlEntity->setValid(true);
                $response['valid'] = true;
                $response['status'] = 200;

                $xml = simplexml_load_file($filepath);

                $response['items'] = count($xml->item);
                $response['filename'] = $xmlName;

                $qb = $entityManager->createQueryBuilder();

                $numberOfRepeats = 0;
                foreach($xml->item as $item){

                    $result = $qb->from("CatalogBackendBundle:Goods", "g")
                        ->select("g.subcategory_id")
                        ->where($qb->expr()->andX(
                            $qb->expr()->eq("g.full_name", ":name"),
                            $qb->expr()->eq("g.brand", ":brand"),
                            $qb->expr()->eq("g.article", ":article"),
                            $qb->expr()->eq("g.subcategory_id", ":category")
                        ))
                        ->setParameters(
                            array(
                                "name" => $item->fullname,
                                "brand" => $item->brand,
                                "article" => $item->article,
                                "category" => $item->category_id
                            )
                        )
                        ->getQuery();


//                  $query = $this->getDoctrine()->getEntityManager()->createQuery(
//                    "SELECT g
//                      FROM CatalogBackendBundle:Goods g
//                      WHERE g.full_name = :name
//                      AND g.brand = :brand
//                      AND g.article = :article"
//                    )
//                        ->setParameters(
//                            array(
//                                'name' => $xml->fullname,
//                                'brand' => $item->brand,
//                                'article' => $item->article
//                            )
//                        );
//                    $result = $query->getOneOrNullResult(Query::HYDRATE_OBJECT);



                    if(!empty($result)){
                        $numberOfRepeats+=1;
                    }
                }

                $response['repeats'] = $numberOfRepeats;

            } else {
                $xmlEntity->setValid(false);
                $response['valid'] = false;
                $response['status'] = 404;
            }
            $response['link'] = $filepath;
            $xmlEntity->setSize(filesize($filepath));
            $xmlEntity->setPath($filepath);
            $xmlEntity->setFilename($xmlName);
            $entityManager->persist($xmlEntity);
            $entityManager->flush();
            $entityManager->clear();

            return new JsonResponse(
                $response, $response['status']
            );
        }


    }


    private
    function removeOldAttrsAndPhotos($item)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $attributes = $entityManager->getRepository('CatalogBackendBundle:GoodsAttribute')->findBy(array('goods' => $item));
        $photos = $entityManager->getRepository('CatalogBackendBundle:GoodsPhoto')->findBy(array('goods' => $item));

        foreach ($attributes as $attribute) {
            $entityManager->remove($attribute);
        }
        $entityManager->flush();
        foreach ($photos as $photo) {
            $entityManager->remove($photo);
        }

        $entityManager->flush();

        return true;

    }


    private
    function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    private
    function getUploadDir()
    {
        return 'images/';
    }

    private function upload($file, $atricle, $brand, $position)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $newFilename = md5(microtime());
        $extention = $file->guessExtension();
        $article = preg_replace('/\s+/', '', $atricle);
        $newFilename = $article . '_' . $position . '.' . $extention;
        $folderNameString = preg_replace('/[^ \w]+/', '', $brand);
        $folderName = $this->getUploadRootDir() . $folderNameString . '/';

        $fs = new Filesystem();
        try {
            $fs->mkdir($folderName, 0777);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at " . $e->getPath();
        }

        $file->move(
            $folderName,
            $newFilename
        );


        return $this->getUploadDir() . $folderNameString . '/' . $newFilename;
    }
}
