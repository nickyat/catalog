<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * XmlRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class XmlRepository extends EntityRepository
{
}
