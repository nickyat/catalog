<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Goods
 */
class Goods
{
    
    public function getFirstImage()
    {
        $firstImage = null;
        $photos = $this->getPhoto();
        if(count($photos) > 1){
        foreach ($photos as $photo){
            if ($photo->getPosition() == 1){
                $firstImage = $photo;
                break;
            }
        }
        }else{
            $firstImage = $photos[0];
        }

        
        return $firstImage;
    }
    
    public function toArray()
    {
        $goods = array();
        $goods['id'] = $this->getId();
        $goods['short_name'] = $this->getBrand() . ' ' .  $this->getArticle();
        $goods['full_name'] = $this->getFullName();
        foreach($this->getPhoto() as $photo){
                $goods['first_image'] = $this->getFirstImage();
            if ($this->getFirstImage() != $photo){
                $goods['photos'][] = $photo;
            }
        }
        $goods['description'] = $this->getDescription();
        $goods['subcategory'] = $this->getSubcategory();
        $attributes = array();
        foreach ($this->getAttribute() as $attr){
            $attributes[$attr->getCategoryAttribute()->getOrders()] = $attr;
        }
        ksort($attributes);
        foreach ($attributes as $attribute){
            if($attribute->getValue()){
                $goods['attributes'][$attribute->getCategoryAttribute()->getGroups()][$attribute->getCategoryAttribute()->getOrders()] = $attribute;
            }
        }
        
        return $goods;
    }
    
    public function findAttribute($categoryAttribute)
    {
        $value = '';
        foreach ($this->getAttribute() as $attribute){
            if ($attribute->getCategoryAttribute()->getId() == $categoryAttribute){
                $value = $attribute->getValue();
            }
        }
        
        return $value;
    }

    /**
     * Get all item attrbiutes using it's category attribute
     * @param null $category_attribute
     * @return array
     */
    public function getAttributesByCategoryAttribute($category_attribute = null)
    {
        $attributes = array();

        foreach ($this->getAttribute() as $attribute){

            if($attribute->getCategoryAttribute()->getId() == $category_attribute)
            {
                $attributes[] = $attribute->getValue();
            }


        }

        return $attributes;
    }

    /**
     * Get category name by it's id attribute
     * @param null $category_attribute
     * @return mixed
     */
    public function getNameByCategoryAttribute($category_attribute = null)
    {

        foreach ($this->getAttribute() as $attribute){

            if($attribute->getCategoryAttribute()->getId() == $category_attribute)
            {
                $name = $attribute->getCategoryAttribute()->getName();
                break;
            }


        }

        return $name;
    }

    /**
     * Get all categories attributes which are used in this attribute items
     */
    public function getCategoryAttributesId()
    {
        $categoryAttributes = array();

        foreach($this->getAttribute() as $attribute)
        {
            $categoryAttributes[] = $attribute->getCategoryAttribute()->getId();
        }
        return array_unique($categoryAttributes);
    }


    public function attributesSorting()
    {
        $attributesArray = array();
        
        foreach ($this->getAttribute() as $attr){
            $attributesArray[(int)$attr->getCategoryAttribute()->getOrders()] = $attr;
        }
         ksort($attributesArray);
        return $attributesArray;
    }


    public function lifecycleFileUpload()
    {
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $short_name;

    /**
     * @var string
     */
    private $full_name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attribute;

  

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->attribute = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function __toString()
    {
        return $this->full_name;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set short_name
     *
     * @param string $shortName
     * @return Goods
     */
    public function setShortName($shortName)
    {
        $this->short_name = $shortName;

        return $this;
    }

    /**
     * Get short_name
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->short_name;
    }

    /**
     * Set full_name
     *
     * @param string $fullName
     * @return Goods
     */
    public function setFullName($fullName)
    {
        $this->full_name = $fullName;

        return $this;
    }

    /**
     * Get full_name
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Goods
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add photo
     *
     * @param \Catalog\BackendBundle\Entity\GoodsPhoto $photo
     * @return Goods
     */
    public function addPhoto(\Catalog\BackendBundle\Entity\GoodsPhoto $photo)
    {
        $photo->setGoods($this);
        $this->photo[] = $photo;
    }
    
    /**
    * Set mac
    *
    * @param Doctrine\Common\Collections\ArrayCollection $photo
    */
    public function setPhoto($photo)
    {
        if (!$photo) {
            $this->photo = new \Doctrine\Common\Collections\ArrayCollection();
            return;
        }

        foreach ($photo as $item) {
            $item->setGoods($this);
        }

//        foreach ($this->getPhoto() as $item) {
//            if (!$photo->contains($item)) {
//                $this->getPhoto()->removeElement($item);
//                $item->setGoods(null);
//            }
//        }

        $this->photo = $photo;
    }

    /**
     * Remove photo
     *
     * @param \Catalog\BackendBundle\Entity\GoodsPhoto $photo
     */
    public function removePhoto(\Catalog\BackendBundle\Entity\GoodsPhoto $photo)
    {
        $this->photo->removeElement($photo);
    }

    /**
     * Get photo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Add attribute
     *
     * @param \Catalog\BackendBundle\Entity\GoodsAttribute $attribute
     * @return Goods
     */
    public function addAttribute(\Catalog\BackendBundle\Entity\GoodsAttribute $attribute)
    {
        $this->attribute[] = $attribute;
        $attribute->setGoods($this);

        return $this;
    }
   
    /**
     * Remove attribute
     *
     * @param \Catalog\BackendBundle\Entity\GoodsAttribute $attribute
     */
    public function removeAttribute(\Catalog\BackendBundle\Entity\GoodsAttribute $attribute)
    {
        $this->attribute->removeElement($attribute);
    }

    /**
     * Get attribute
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

   
    private $rating;


    /**
     * Set rating
     *
     * @param integer $rating
     * @return Goods
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }
    /**
     * @var string
     */
    private $brand;

    /**
     * @var string
     */
    private $article;


    /**
     * Set brand
     *
     * @param string $brand
     * @return Goods
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set article
     *
     * @param string $article
     * @return Goods
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return string 
     */
    public function getArticle()
    {
        return $this->article;
    }
    /**
     * @var \Catalog\BackendBundle\Entity\Category
     */
    private $subcategory;


    /**
     * Set subcategory
     *
     * @param \Catalog\BackendBundle\Entity\Category $subcategory
     * @return Goods
     */
    public function setSubcategory(\Catalog\BackendBundle\Entity\Category $subcategory = null)
    {
        $this->subcategory = $subcategory;

        return $this;
    }

    /**
     * Get subcategory
     *
     * @return \Catalog\BackendBundle\Entity\Category 
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }
}
