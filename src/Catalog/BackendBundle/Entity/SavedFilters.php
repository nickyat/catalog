<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SavedFilters
 */
class SavedFilters
{
    
    public function toArray($subcategoryId)
    {
        $filters = $this->getFiltersJson();
        $response = array();
        $filters = unserialize($filters);
        if (isset($filters[$subcategoryId])){
            foreach ($filters[$subcategoryId] as $name => $value){
                foreach ($value as $n => $b){
                    if ($b == true){
                        $response[] = $n;
                    }
                }
            }
        }else{
            $response = array();
        }
        return $response;
    }


    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var string
     */
    private $filters_json;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return SavedFilters
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set filters_json
     *
     * @param string $filtersJson
     * @return SavedFilters
     */
    public function setFiltersJson($filtersJson)
    {
        $this->filters_json = $filtersJson;

        return $this;
    }

    /**
     * Get filters_json
     *
     * @return string 
     */
    public function getFiltersJson()
    {
        return $this->filters_json;
    }
}
