<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
/**
 * Category
 */
class Category
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
     * @var string
     */
    private $name;

    public function generateSiteMap()
    {
        $stop = false;
        $sitemap = array($this);
        $category = $this;
        while ($stop == false){
            if ($category->getParent()){
                $category = $category->getParent();
                $sitemap[] = $category;
            }else{
                $stop = true;
            }
        
        }
        
        return array_reverse($sitemap);
    }
    
    public function getAllBrands()
    {
        $brands = array();
        foreach ($this->getGoods() as $good)
        {
            $brands[] = $good->getBrand();
        }
        asort($brands);
        return $brands;
    }


    public function getGoodsCount()
    {
        $stop = false;
        $category = $this;
        $goodsCount = count($this->getGoods());
        while ($stop == false){
            if ($category->getParent()){
                $category = $category->getParent();
                $goodsCount+= count($category->getGoods());
            }else{
                $stop = true;
            }
        
        }
        return $goodsCount;
    }



    public function __toString(){
        return $this->name;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $subcategory;
    
    protected $request;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subcategory = new \Doctrine\Common\Collections\ArrayCollection();
    }

   
    private $image_path;


    /**
     * Set image_path
     *
     * @param string $imagePath
     * @return Category
     */
    public function setImagePath($imagePath)
    {
        $this->image_path = $imagePath;

        return $this;
    }

    /**
     * Get image_path
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->image_path;
    }
    
    
     /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
     public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return 'http://sb.avantkursk.ru/Catalog/web/images/';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'images/';
    }
    public function lifecycleFileUpload() {
        $this->upload();
    }
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->getFile()->getClientOriginalName()
        );

        $filename = $this->getFile()->getClientOriginalName();
        $this->image_path = $this->getWebPath() . $filename;

        $this->file = null;
    }
        /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $goods;

    /**
     * @var \Catalog\BackendBundle\Entity\Category
     */
    private $parent;


    /**
     * Add goods
     *
     * @param \Catalog\BackendBundle\Entity\Goods $goods
     * @return Category
     */
    public function addGood(\Catalog\BackendBundle\Entity\Goods $goods)
    {
        $this->goods[] = $goods;

        return $this;
    }

    /**
     * Remove goods
     *
     * @param \Catalog\BackendBundle\Entity\Goods $goods
     */
    public function removeGood(\Catalog\BackendBundle\Entity\Goods $goods)
    {
        $this->goods->removeElement($goods);
    }

    /**
     * Get goods
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * Set parent
     *
     * @param \Catalog\BackendBundle\Entity\Category $parent
     * @return Category
     */
    public function setParent(\Catalog\BackendBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Catalog\BackendBundle\Entity\Category 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add subcategory
     *
     * @param \Catalog\BackendBundle\Entity\Category $subcategory
     * @return Category
     */
    public function addSubcategory(\Catalog\BackendBundle\Entity\Category $subcategory)
    {
        $this->subcategory[] = $subcategory;

        return $this;
    }

    /**
     * Remove subcategory
     *
     * @param \Catalog\BackendBundle\Entity\Category $subcategory
     */
    public function removeSubcategory(\Catalog\BackendBundle\Entity\Category $subcategory)
    {
        $this->subcategory->removeElement($subcategory);
    }

    /**
     * Get subcategory
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attribute;


    /**
     * Add attribute
     *
     * @param \Catalog\BackendBundle\Entity\CategoryAttribute $attribute
     * @return Category
     */
    public function addAttribute(\Catalog\BackendBundle\Entity\CategoryAttribute $attribute)
    {
        $this->attribute[] = $attribute;
        $attribute->setCategory($this);

        return $this;
    }

    /**
     * Remove attribute
     *
     * @param \Catalog\BackendBundle\Entity\CategoryAttribute $attribute
     */
    public function removeAttribute(\Catalog\BackendBundle\Entity\CategoryAttribute $attribute)
    {
        $this->attribute->removeElement($attribute);
    }

    /**
     * Get attribute
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
}
