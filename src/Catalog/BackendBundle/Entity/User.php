<?php
/**
 * Created by PhpStorm.
 * User: md760b
 * Date: 05.11.14
 * Time: 12:36
 */

namespace Catalog\BackendBundle\Entity;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    protected $id;
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}