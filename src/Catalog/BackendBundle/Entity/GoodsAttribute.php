<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GoodsAttribute
 */
class GoodsAttribute
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $value;

    /**
     * @var \Catalog\BackendBundle\Entity\CategoryAttribute
     */
    private $category_attribute;


    public function __toString()
    {
        return $this->value;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return GoodsAttribute
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set category_attribute
     *
     * @param \Catalog\BackendBundle\Entity\CategoryAttribute $categoryAttribute
     * @return GoodsAttribute
     */
    public function setCategoryAttribute(\Catalog\BackendBundle\Entity\CategoryAttribute $categoryAttribute = null)
    {
        $this->category_attribute = $categoryAttribute;

        return $this;
    }

    /**
     * Get category_attribute
     *
     * @return \Catalog\BackendBundle\Entity\CategoryAttribute 
     */
    public function getCategoryAttribute()
    {
        return $this->category_attribute;
    }
    /**
     * @var \Catalog\BackendBundle\Entity\Goods
     */
    private $goods;


    /**
     * Set goods
     *
     * @param \Catalog\BackendBundle\Entity\Goods $goods
     * @return GoodsAttribute
     */
    public function setGoods(\Catalog\BackendBundle\Entity\Goods $goods = null)
    {
        $this->goods = $goods;

        return $this;
    }

    /**
     * Get goods
     *
     * @return \Catalog\BackendBundle\Entity\Goods
     */
    public function getGoods()
    {
        return $this->goods;
    }


}
