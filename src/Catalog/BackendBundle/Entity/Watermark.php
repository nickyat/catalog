<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
/**
 * Watermark
 */
class Watermark
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $image_url;

    /**
     * @var string
     */
    private $image_path;

    /**
     * @var string
     */
    private $position;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image_url
     *
     * @param string $imageUrl
     * @return Watermark
     */
    public function setImageUrl($imageUrl)
    {
        $this->image_url = $imageUrl;

        return $this;
    }

    /**
     * Get image_url
     *
     * @return string 
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * Set image_path
     *
     * @param string $imagePath
     * @return Watermark
     */
    public function setImagePath($imagePath)
    {
        $this->image_path = $imagePath;

        return $this;
    }

    /**
     * Get image_path
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->image_path;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Watermark
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }
    /**
     * @ORM\PrePersist
     */
   public function lifecycleFileUpload()
    {
        $this->upload();
    }
    
    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
     public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return 'http://sb.avantkursk.ru/Catalog/web/images/';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'images/';
    }

    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }
        $watermarkFilename = 'watermark' . '.jpeg';
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $watermarkFilename
        );

        $this->image_url = $this->getWebPath() . $watermarkFilename;
        $this->setImagePath($this->getUploadRootDir() . $watermarkFilename);
        $this->file = null;
        
        $fs = new Filesystem();
        $fs->remove(__DIR__.'/../../../../web/media/cache/');
    }
}
