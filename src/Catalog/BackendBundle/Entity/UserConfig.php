<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Catalog\BackendBundle\Entity\Category;

/**
 * Config
 */
class UserConfig
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $session;

    /**
     * @var integer
     */
    private $category;

    /**
     * @var array
     */
    private $brand;

    /**
     * @var array
     */
    private $attribute;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $ip;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set session
     *
     * @return Config
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string 
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set category
     *
     * @param Category $category
     * @return Config
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brands
     *
     * @param \stdClass $brand
     * @return Config
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brands
     *
     * @return \stdClass 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set attributes
     *
     * @param array $attribute
     * @return Config
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute
     *
     * @return array 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }


    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    public function getIp()
    {
        return $this->ip;
    }
}
