<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Catalog\BackendBundle\Entity\GoodsPhoto;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Liip\ImagineBundle\Imagine\Filter\Loader\ThumbnailFilterLoader;
use Imagine\Gd\Image;
use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

class PhotoListener
{
    protected $entityManager;



    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return 'http://sb.avantkursk.ru/Catalog/web/images/';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'images/';
    }
    public function lifecycleFileUpload(GoodsPhoto $photo,LifecycleEventArgs $event) {
        $this->entityManager = $event->getEntityManager();
        $this->upload($photo);
    }
    public function upload($photo)
    {
        if (null === $photo->getFile()) {
            return;
        }
        
        $newFilename = md5(microtime());
        $extention = $photo->getFile()->guessExtension();
        $newFilename = $newFilename . '.' . $extention;
        
        $photo->getFile()->move(
            $this->getUploadRootDir(),
            $newFilename
        );
        $imagine  = new Imagine();
        $watermarkObject = $this->entityManager->getRepository('CatalogBackendBundle:Watermark')->findAll();
        $watermarkPath = $watermarkObject[0]->getImagePath();
        $watermark = $imagine->open($watermarkPath);
        $image     = $imagine->open($this->getUploadRootDir() . $newFilename);
        $size      = $image->getSize();
        $wSize     = $watermark->getSize();
        
        switch ($watermarkObject[0]->getPosition()){
            case 'center':
                $position = new Point($size->getWidth() / 2 , $size->getHeight() / 2);
            break;
            case 'left-top':
                $position = new Point(0, 0);
            break;
            case 'rigth-top':
                $position = new Point($size->getWidth() - $wSize->getWidth(), 0);
            break;
            case 'left-bottom':
                $position = new Point(0, $size->getHeight() - $wSize->getHeight());
            break;
            case 'right-bottom':
                $position = new Point($size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight());
            break;
        
        }
        
        $result = $image->paste($watermark, $position);
        
        $image->save($this->getUploadRootDir() . $newFilename);
        
        $photo->setPath($this->getWebPath() . $newFilename);
        
        
        $photo->setFile(null);
        
        
    }
    
    
    public function prePersist(LifecycleEventArgs $event)
    {
        
    }
}