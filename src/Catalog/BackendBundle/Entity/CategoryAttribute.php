<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryAttribute
 */
class CategoryAttribute
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $groups;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $show_in_list;

    /**
     * @var boolean
     */
    private $show_in_filter;

    /**
     * @var string
     */
    private $validator;

    /**
     * @var \Catalog\BackendBundle\Entity\Category
     */
    private $category;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groups
     *
     * @param string $groups
     * @return CategoryAttribute
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * Get groups
     *
     * @return string 
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CategoryAttribute
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set show_in_list
     *
     * @param boolean $showInList
     * @return CategoryAttribute
     */
    public function setShowInList($showInList)
    {
        $this->show_in_list = $showInList;

        return $this;
    }

    /**
     * Get show_in_list
     *
     * @return boolean 
     */
    public function getShowInList()
    {
        return $this->show_in_list;
    }

    /**
     * Set show_in_filter
     *
     * @param boolean $showInFilter
     * @return CategoryAttribute
     */
    public function setShowInFilter($showInFilter)
    {
        $this->show_in_filter = $showInFilter;

        return $this;
    }

    /**
     * Get show_in_filter
     *
     * @return boolean 
     */
    public function getShowInFilter()
    {
        return $this->show_in_filter;
    }

    /**
     * Set validator
     *
     * @param string $validator
     * @return CategoryAttribute
     */
    public function setValidator($validator)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return string 
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Set category
     *
     * @param \Catalog\BackendBundle\Entity\Category $category
     * @return CategoryAttribute
     */
    public function setCategory(\Catalog\BackendBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Catalog\BackendBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }



  
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $goods_attribute;


    /**
     * Add goods_attribute
     *
     * @param \Catalog\BackendBundle\Entity\GoodsAttribute $goodsAttribute
     * @return CategoryAttribute
     */
    public function addGoodsAttribute(\Catalog\BackendBundle\Entity\GoodsAttribute $goodsAttribute)
    {
        $this->goods_attribute[] = $goodsAttribute;

        return $this;
    }

    /**
     * Remove goods_attribute
     *
     * @param \Catalog\BackendBundle\Entity\GoodsAttribute $goodsAttribute
     */
    public function removeGoodsAttribute(\Catalog\BackendBundle\Entity\GoodsAttribute $goodsAttribute)
    {
        $this->goods_attribute->removeElement($goodsAttribute);
    }

    /**
     * Get goods_attribute
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGoodsAttribute()
    {
        return $this->goods_attribute;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->goods_attribute = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var integer
     */
    private $position;


    /**
     * Set position
     *
     * @param integer $position
     * @return CategoryAttribute
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
    /**
     * @var integer
     */
    private $order;


    /**
     * Set order
     *
     * @param integer $order
     * @return CategoryAttribute
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * @var integer
     */
    private $orders;


    /**
     * Set orders
     *
     * @param integer $orders
     * @return CategoryAttribute
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * Get orders
     *
     * @return integer 
     */
    public function getOrders()
    {
        return $this->orders;
    }
    /**
     * @var string
     */
    private $validator_string;


    /**
     * Set validator_string
     *
     * @param string $validatorString
     * @return CategoryAttribute
     */
    public function setValidatorString($validatorString)
    {
        $this->validator_string = $validatorString;

        return $this;
    }

    /**
     * Get validator_string
     *
     * @return string 
     */
    public function getValidatorString()
    {
        return $this->validator_string;
    }
    
    public function getAllowedValues()
    {
        $allowedValues = array();
        if ($this->getValidator() == 'range'){
            $values = explode(',',$this->getValidatorString());
            foreach ($values as $value){
                $explode = explode('-', $value);
                if (count($explode) > 1){
                    $val = explode('-', $value);
                    for ($i=$val[0]; $i<= $val[1]; $i++){
                        $allowedValues[]= $i;
                    }
                }else{
                    $allowedValues[] = $value;
                }
            }
            return $allowedValues;
        }

    }
}
