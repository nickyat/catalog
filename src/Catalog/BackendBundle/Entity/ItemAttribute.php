<?php

namespace Catalog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ItemAttribute
 */
class ItemAttribute
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $good;

    /**
     * @var integer
     */
    private $attribute;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set good
     *
     * @param integer $good
     * @return ItemAttribute
     */
    public function setGood(Goods $good)
    {
        $this->good = $good;

        return $this;
    }

    /**
     * Get good
     *
     * @return integer 
     */
    public function getGood()
    {
        return $this->good;
    }

    /**
     * Set attribute
     *
     * @param integer $attribute
     * @return ItemAttribute
     */
    public function setAttribute(GoodsAttribute $attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute
     *
     * @return integer 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
}
