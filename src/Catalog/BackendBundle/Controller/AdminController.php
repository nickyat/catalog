<?php
namespace Catalog\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Catalog\BackendBundle\Entity\Category;
use Catalog\BackendBundle\Entity\SubCategory;
use Catalog\BackendBundle\Entity\Goods;
use Catalog\BackendBundle\Entity\GoodsPhoto;
use Catalog\BackendBundle\Entity\GoodsAttribute;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminController extends Controller
{
    public function generateAttributesAction(Request $request, $categoryId, $goodsId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
        if($goodsId != 0){
            
            $categoryAttributes = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->findBy(array('category' => $categoryId));
            $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->find($goodsId);
            
            return $this->render('CatalogAdminBundle:CRUD:add_attributes.html.twig',
                    array('attributes' => $categoryAttributes,'goods' => $goods,
                    array('id' => 'ASC')));
        }else{
            $categoryAttributes = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->findBy(
                    array('category' => $categoryId),
                    array('id' => 'ASC'));
        
            return $this->render('CatalogAdminBundle:CRUD:add_attributes.html.twig',
                    array('attributes' => $categoryAttributes));
        }
        $request->files->all();
    }
    
    public function generatePhotoContainerAction(Request $request, $goodsId)
    {   
         $entityManager = $this->getDoctrine()->getManager();
        
        if($goodsId != 0){
            $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->find($goodsId);
            $goodsPhoto = $entityManager->getRepository('CatalogBackendBundle:GoodsPhoto')->findBy(array('goods' => $goods));
            return $this->render('CatalogAdminBundle:CRUD:add_photo.html.twig',array('photos' => $goodsPhoto));
            
        }else{
            return $this->render('CatalogAdminBundle:CRUD:add_photo.html.twig');
        }
    }
    
    public function generatePhotoFieldAction(Request $request, $position)
    {
        return $this->render('CatalogAdminBundle:CRUD:add_photo_field.html.twig', array('position' => $position));
    }
    
    public function loadTemporalImageAction(Request $request, $position)
    {
        $file = $request->files->get('photos');
        $file = $file['photo'][$position];
        $file->move(
            $this->getUploadRootDir(),
            $file->getClientOriginalName()
        );
        
        return new Response('http://'.$request->getHost() . '/Catalog/web/images/'.$file->getClientOriginalName());
    }
    
     protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'images/';
    }
    
    
}