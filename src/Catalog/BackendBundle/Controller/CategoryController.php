<?php
namespace Catalog\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Catalog\BackendBundle\Entity\Category;
use Catalog\BackendBundle\Entity\SubCategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class CategoryController extends Controller
{
    //Получение списка категорий
    public function getListCategoriesAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        // Достаются все категории отсортированные в алфавитном порядке по названию
        $allCategories = $entityManager->getRepository('CatalogBackendBundle:Category')->findBy(
                array('parent' => null),
                array('name' => 'ASC'));
        return $this->render('CatalogFrontendBundle:Category:all_categories.html.twig', array(
            'categories' => $allCategories
        ));
    }
    
    /* Объединить верхний и нижний методы */
    
    //Ajax загрузка всех категорий
    public function ajaxLoadCategoriesAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
        $allCategories = $entityManager->getRepository('CatalogBackendBundle:Category')->findBy(
                array('parent' => null),
                array('name' => 'ASC'));
        
        return $this->render('CatalogFrontendBundle:Category:ajax_load_categories.html.twig', array(
            'categories' => $allCategories
        ));
    }
 // Поиск субкатегорий по категории
    public function getListSubcategoriesAction(Request $request, $categoryId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
        $categories = $entityManager->getRepository('CatalogBackendBundle:Category')->findBy(array('parent' => $categoryId));
        if ($categories){
            return $this->render('CatalogFrontendBundle:Category:ajax_load_categories.html.twig', array(
                'categories' => $categories,
                'parent' => $categories[0]->getParent()
            ));
        }else{
            return $this->render('CatalogFrontendBundle:Category:subcategories.html.twig', array(
                'subcategories' => $categories,
                'category' => $categories[0]->getParent()
            ));
        }
    }

    
}