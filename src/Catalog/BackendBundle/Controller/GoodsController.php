<?php
namespace Catalog\BackendBundle\Controller;

use Catalog\BackendBundle\Entity\SubCategory;
use Catalog\BackendBundle\Entity\UserConfig;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Catalog\BackendBundle\Entity\SavedFilters;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class GoodsController extends Controller
{
    //TODO если удалить кукисы а запись в бд будет то вылетает ошибка
    const PER_PAGE = 10;
    const ORDER = 'brand';

    /**
     * //TODO удалить этот метод
     * @return string
     */
    public function ajaxTestAction()
    {
        return new Response('{"bInPrice":1,"sData":"5 799,91 from: 1 day."}');
    }
    /**
     * TODO: удалить этот метод
     * @param Request $request
     */
    public function testAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userIp = $request->getClientIp();
        $brand = "Honda";
        $userConfig = $em->getRepository('CatalogBackendBundle:UserConfig')
            ->findOneByIpOrSession(86, $userIp, null);

        $allBrands = $userConfig->getBrand();


        $searchResult = array_search($brand, $allBrands);

        if(!empty($searchResult)){
            unset($allBrands[$searchResult]);
        }

        echo '<pre>';
        var_dump($allBrands);
        echo '</pre>';

        exit;
    }


    //Получение списка товаров по субкатегории
    /**
     * @param Request $request
     * @param $subcategoryId
     * @return Response
     */
    public function getGoodsListBySubcategoryAction(Request $request, $subcategoryId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $cookieExists = $request->cookies->get('order');

        if(empty($cookieExists)){
            $cookies = $this->setGoodsOrderInCookie($request, self::PER_PAGE, self::ORDER, $subcategoryId);
        }else{
            $cookiePerPage = $request->get('per_page');
            $cookieOrder = $request->get('order');


            $cookies = unserialize($cookieExists);
            if(!empty($cookieOrder) || !empty($cookiePerPage))
            {
                if(empty($cookieOrder)){
                    $cookieOrder = $cookies['order'];
                }

                if(empty($cookiePerPage)){
                    $cookiePerPage = $cookies['per_page'];
                }

                $cookies = $this->setGoodsOrderInCookie($request, $cookiePerPage, $cookieOrder, $subcategoryId);
            }else{
                if($cookies['order'] == NULL || $cookies['per_page'] == NULL)
                {
                    $cookies = $this->setGoodsOrderInCookie($request, self::PER_PAGE, self::ORDER, $subcategoryId);
                }else{
                    $cookies = $this->setGoodsOrderInCookie($request, $cookies['per_page'], $cookies['order'], $subcategoryId);

                }
            }
        }


        $goodsPerPage = $cookies['per_page'];
        $goodsOrder = $cookies['order'];

            // echo $goodsPerPage;
           // echo $goodsOrder;

        $attributeNames = array();
        $attributeValues = array();
        $combinedValues = array();

        $goodsPage = $request->get('page');
        $filters = $request->get('filters');
        $brand = $request->get('brand');


        $userIp = $this->container->get('request')->getClientIp();

        $userConfig = $entityManager->getRepository('CatalogBackendBundle:UserConfig')
            ->findOneByIpOrSession($subcategoryId, $userIp, $cookies['session']);

            $allBrands = $userConfig->getBrand();
            if(!empty($allBrands)){

                if(!empty($brand)){
                    if(in_array($brand, $allBrands) == false){
                       // array_push($allBrands, $brand);
                        $userConfig->setBrand($brand);
                        $entityManager->persist($userConfig);
                        $entityManager->flush();
                    }
                }
            }else{
                $allBrands = array($brand);
                $userConfig->setBrand($allBrands);
                $entityManager->persist($userConfig);
                $entityManager->flush();
            }


//            if(empty($allBrands) && !empty($brand))
//            {
//                $userConfig->setBrand(array($brand));
//            }
//
//            if(!empty($allBrands) && is_array($allBrands)){
//                var_dump($allBrands);
//            }
//
//
//            if(!empty($brand)){
//                $userConfig->setBrand(array_push($allBrands, $brand));
//            }
//
//        if(!empty($brand)){
//            if(in_array($brand, $allBrands)){
//                echo 'found';
//            }else{
//                echo 'not found';
//                $userConfig->setBrand();
//                $entityManager->persist($userConfig);
//                $entityManager->flush();
//            }
//
//        }










        if ($filters) {
            foreach ($filters as $filter) {
                foreach ($filter as $name => $value) {
                    if ($value != 'all') {
                        $explode = explode('|', $value);
                        if (count($explode) > 1) {
                            foreach ($explode as $ex) {
                                $combinedValues[$ex] = $ex;
                            }
                        }
                        $attributeNames[] = $name;
                        $attributeValues[] = $value;
                    }
                }
            }
//            echo '<pre>';
//            var_dump($filters);
//            echo '</pre>';
//
//            echo '<pre>';
//            var_dump($brand);
//            echo '</pre>';
        }






        if (!$goodsOrder){
            $goodsOrder = $cookies['order'];
        }
        if (!$goodsPerPage){

            $goodsPerPage = $cookies['per_page'];
        }

        if (!$goodsPage) {
            $goodsPage = 1;
        }


        $from = ($goodsPerPage * $goodsPage) - $goodsPerPage;
        $subcategory = $entityManager->getRepository('CatalogBackendBundle:Category')->find($subcategoryId);


        foreach($allBrands as $key => $value){
            if($value == null || $value == 'all'){
                unset($allBrands[$key]);
            }
        }

        $userAttribute = $userConfig->getAttribute();
        $userValues = $userConfig->getValue();




        if ( (!empty($attributeNames) && !empty($attributeValues))
            || (!empty($userAttribute) && !empty($userValues))
        ) {
            if(empty($attributeNames) && !empty($userAttribute))
            {
                $attributeNames = $userAttribute;
            }

            if(empty($attributeValues) && !empty($userValues))
            {
                $attributeValues = $userValues;
            }

            $userConfig->setValue($attributeValues);
            $userConfig->setAttribute($attributeNames);
            $entityManager->persist($userConfig);
            $entityManager->flush();

            if (count($allBrands) > 0) {
                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')
                    ->findByAttributesAndBrand(
                        array(
                            'names' => $attributeNames,
                            'values'    => $attributeValues
                        ),
                        $subcategoryId,
                        $allBrands,
                        array(
                            'order' => $goodsOrder,
                            'limit' => $goodsPerPage,
                            'offset' => $from
                        )
                    );
            } else {

                $arrayDefault = count($attributeValues);

                $all = $this->getDoctrine()->getManager()->getRepository('CatalogBackendBundle:Goods')
                    ->findByAttribute($attributeValues, $subcategoryId);


                $goods = array();
                $md5Array = array();
                foreach($all as $item){

                    $arrayOfAttributes = array();

                    foreach($item->getGoods()->getAttribute() as $attribute){

                        $arrayOfAttributes[] = $attribute->getValue();

                    }

                    $intersect = array_intersect($attributeValues, $arrayOfAttributes);

                    if(count($intersect) >= $arrayDefault ){
                        $md5name = md5($item->getGoods()->getArticle());

                        if(!in_array($md5name, $md5Array)){
                            $md5Array[] = $md5name;
                            $goods[] = $item->getGoods();
                        }

                    }

                }


               // $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')
               //     ->findByAttributes($attributeNames, $attributeValues, $subcategoryId, $goodsOrder);
            }

        } else {

            //if(count($allBrands) > 0){

                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')
                    ->findByBrandsAndCategory($allBrands, $subcategoryId, $goodsPerPage, $from);
           // }
//            if ($brand && $brand != 'all') {
//
//                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findBy(
//                    array('subcategory' => $subcategory,
//                          'brand'       => $brand,
//                    ),
//                    array($goodsOrder => 'DESC'),
//                    $goodsPerPage,
//                    $from
//                );
//            } else {
//
//                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findBy(
//                    array('subcategory' => $subcategory),
//                    array($goodsOrder => 'DESC'),
//                    $goodsPerPage,
//                    $from
//                );
//            }
        }

//        if(empty($brand) || $brand == 'all'){
//
//            $query = array(
//                'subcategory' => $subcategory
//            );
//
//        }else{
//            $query = array(
//                'subcategory' => $subcategory,
//                'brand' => $brand
//            );
//        }
//
//        $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findBy(
//            $query,
//            array($goodsOrder => 'DESC'),
//            $goodsPerPage,
//            $from
//        );

//        for ($i = $from; $i < $from + $goodsPerPage; $i++) {
//            if (isset($goods[$i]) && $goods[$i]) {
//                $goodsArray[] = $goods[$i];
//            }
//        }
        $enabledFilters = $this->getEnabledFilters($request, $subcategoryId);
        if ($enabledFilters) {
            $attributes = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->findBy(array('category' => $subcategory, 'name' => $enabledFilters));
        } else {
            $attributes = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->findBy(array('category' => $subcategory, 'show_in_filter' => true));
        } 



        $attributesArray = array();
        $goodsAttributes = $entityManager->getRepository('CatalogBackendBundle:GoodsAttribute')->findBy(
            array(
                'goods' => $goods,
                'category_attribute' => $attributes
            ),
            array(
                'value' => 'ASC'
                )
            );

        if($goodsAttributes){



            foreach ($goodsAttributes as $attribute) {
                if (trim($attribute->getValue())) {
                    $combined = explode('|', $attribute->getValue());
                    if (count($combined) > 1) {
                        foreach ($combined as $value) {
                            $attributesArray[$attribute->getCategoryAttribute()->getName()][$value] = $value;
                        }
                    } else {
                        $attributesArray[$attribute->getCategoryAttribute()->getName()][$attribute->getValue()] = $attribute->getValue();
                    }
                }
            }
        }


        $brands = $entityManager
            ->getRepository('CatalogBackendBundle:Goods')
            ->findByDistinctBrands($subcategoryId);

        $counter = $entityManager
            ->getRepository('CatalogBackendBundle:Goods')
            ->findByNumberInCategory($subcategoryId);


        return $this->render('CatalogFrontendBundle:Goods:list.html.twig', array(
            'goods'          => $goods,
            'subcategory'    => $subcategory,
            'attributes'     => $attributesArray,
            'all_goods'      => $goods,
            'selected_brand' => $brand,
            'selectedNames'  => $attributeNames,
            'selectedValues' => $attributeValues,
            'per_page'       => $goodsPerPage,
            'order'          => $goodsOrder,
            'page'           => $goodsPage,
            'brands'         => $brands,
            'goods_count'    => $counter,
            'allBrands'      => $allBrands,
            'session'        => $userConfig->getSession(),
            'userAttributes' => $userConfig->getAttribute(),
            'userValues'     => $userConfig->getValue()
            ));
    }


    /**
     * @param Request $request
     * @param $subcategoryId
     * @return Response
     */
    public function ajaxLoadGoodsAction(Request $request, $subcategoryId)
    {
        $entityManager = $this->getDoctrine()->getManager();


        $goodsCount = $request->query->get('per_page');
        $subcategory = $entityManager->getRepository('CatalogBackendBundle:Category')->find($subcategoryId);
        if ($subcategory) {
            $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findBy(
                array('subcategory' => $subcategory),
                array(),
                $goodsCount
            );
        }

        return $this->render('CatalogFrontendBundle:Goods:ajax_load_goods.html.twig', array(
            'goods'          => $goods,
            'subcategory'    => $subcategory,
//            'selectedNames'  => $attributeNames,
//            'selectedValues' => $attributeValues,
        ));
    }

    // Получение товара по его Id
    /**
     * @param Request $request
     * @param $goodsId
     * @return Response
     */
    public function getGoodsByIdAction(Request $request, $goodsId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->find($goodsId);

        return $this->render('CatalogFrontendBundle:Goods:item.html.twig', array(
            'subcategory' => $goods->getSubcategory(),
            'goods'       => $goods,
        ));
    }

    //Поиск товара по его наименованию
    /**
     * @param Request $request
     * @param $subcategoryId
     * @return Response
     */
    public function getGoodsByNameAndFullnameAction(Request $request, $subcategoryId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $searchString = $request->get('search_string');
        $goods = array();
        if (strlen(trim($searchString)) > 0) {
            $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findByNameOrFullname($searchString, $subcategoryId);
        }

        return $this->render('CatalogFrontendBundle:Goods:ajax_search_goods.html.twig', array(
            'goods' => $goods,
        ));
    }


    //Поиск товаров по аттрибутам
    /**
     * @param Request $request
     * @param $subcategoryId
     * @return Response
     */
    public function getGoodsFilterByAttributesAction(Request $request, $subcategoryId)
    {

        $response = new Response();
        $enabledFilters = $this->getEnabledFilters($request, $subcategoryId);







        $filters = $request->get('filters');
        $brand = $request->get('brand');
        $attrbutesFilters = $request->get('filters_settings');

        if (isset($attrbutesFilters) && $attrbutesFilters) {
            $this->saveFilters($request, $subcategoryId, $attrbutesFilters);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $attributeNames = array();
        $attributeValues = array();
        if ($filters) {
            foreach ($filters as $filter) {
                foreach ($filter as $name => $value) {
                    if ($value != 'all') {
                        $attributeNames[] = $name;
                        $attributeValues[] = $value;
                    }
                }
            }
        }
        $subcategory = $entityManager->getRepository('CatalogBackendBundle:Category')->find($subcategoryId);




        $cookieExists = $request->cookies->get('order');

        if(empty($cookieExists)){
            $cookies = $this->setGoodsOrderInCookie($request, self::PER_PAGE, self::ORDER, $subcategoryId);
        }else{
            $cookiePerPage = $request->get('per_page');
            $cookieOrder = $request->get('order');


            $cookies = unserialize($cookieExists);
            if(!empty($cookieOrder) || !empty($cookiePerPage))
            {
                if(empty($cookieOrder)){
                    $cookieOrder = $cookies['order'];
                }

                if(empty($cookiePerPage)){
                    $cookiePerPage = $cookies['per_page'];
                }

                $cookies = $this->setGoodsOrderInCookie($request, $cookiePerPage, $cookieOrder, $subcategoryId);
            }else{
                if($cookies['order'] == NULL || $cookies['per_page'] == NULL)
                {
                    $cookies = $this->setGoodsOrderInCookie($request, self::PER_PAGE, self::ORDER, $subcategoryId);
                }else{
                    $cookies = $this->setGoodsOrderInCookie($request, $cookies['per_page'], $cookies['order'], $subcategoryId);

                }
            }
        }


        //$orders = $this->setGoodsOrderInCookie($request, null, null);


        if (!empty($attributeNames) && !empty($attributeValues)) {
            if ($brand && $brand != 'all') {

                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findByAttributesAndBrand($attributeNames, $attributeValues, $subcategoryId, $brand);
            } else {
                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findByAttributes($attributeNames, $attributeValues, $subcategoryId);
            }

        } else {
            if ($brand && $brand != 'all') {
                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findBy(array('subcategory' => $subcategory, 'brand' => $brand));
            } else {
                $goods = $entityManager->getRepository('CatalogBackendBundle:Goods')->findBy(array('subcategory' => $subcategory));
            }
        }
        $enabledFilters = $this->getEnabledFilters($request, $subcategoryId);
        if ($enabledFilters) {
            $attributes = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->findBy(array('category' => $subcategory, 'name' => $enabledFilters));
        } else {
            $attributes = $entityManager->getRepository('CatalogBackendBundle:CategoryAttribute')->findBy(array('category' => $subcategory, 'show_in_filter' => true));
        }

        $attributesArray = array();
        $attributes = $entityManager->getRepository('CatalogBackendBundle:GoodsAttribute')->findBy(array(
            'goods'              => $goods,
            'category_attribute' => $attributes
        ));
        foreach ($attributes as $attribute) {
            $attributesArray[$attribute->getCategoryAttribute()->getName()][$attribute->getValue()] = $attribute->getValue();
        }

        return $this->render('CatalogFrontendBundle:Goods:list.html.twig', array(
            'goods'          => $goods,
            'subcategory'    => $subcategory,
            'attributes'     => $attributesArray,
            'selected_brand' => $brand,
            'selectedNames'  => $attributeNames,
            'selectedValues' => $attributeValues,
            'per_page'       => $cookies['per_page'],
            'order'          => $cookies['order'],
            'page'           => 1,
            'goods_count'    => count($goods)
        ));

    }

    /**
     * @param Request $request
     * @param $subcategoryId
     * @return Response
     */
    public function filterSettingsAction(Request $request, $subcategoryId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $clientIp = $request->getClientIp();
        $enabledFilters = $this->getEnabledFilters($request, $subcategoryId);
        $attributes = $entityManager->getRepository('CatalogBackendBundle:GoodsAttribute')->getAttributesBySubcategory($subcategoryId);
        $attributesArray = array();
        foreach ($attributes as $key => $attribute) {
            $attributesArray[$attribute->getCategoryAttribute()->getName()]['attribute'] = $attribute;
            if ($enabledFilters) {
                $attributesArray[$attribute->getCategoryAttribute()->getName()]['checked'] = (in_array($attribute->getCategoryAttribute()->getName(), $enabledFilters)) ? 'checked' : '';
            } else {
                $attributesArray[$attribute->getCategoryAttribute()->getName()]['checked'] = $attribute->getCategoryAttribute()->getShowInFilter() ? 'checked' : '';
            }
        }

        return $this->render('CatalogFrontendBundle:Goods:filter_settings.html.twig', array(
            'attributes'  => $attributesArray,
            'subcategory' => $subcategoryId
        ));

    }

    /**
     * @param $request
     * @param $subcategoryId
     * @return array
     */
    private function getEnabledFilters($request, $subcategoryId)
    {

        $clientIp = $request->getClientIp();
        $entityManager = $this->getDoctrine()->getManager();
        $filters = $entityManager->getRepository('CatalogBackendBundle:SavedFilters')->findOneBy(array('ip' => $clientIp));
        if ($filters) {
            return $filters->toArray($subcategoryId);
        } else {
            return array();
        }

    }

    /**
     * @param $request
     * @param $subcategoryId
     * @param $attrbutesFilters
     */
    private function saveFilters($request, $subcategoryId, $attrbutesFilters)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $clientIp = $request->getClientIp();
        $filters = $entityManager->getRepository('CatalogBackendBundle:SavedFilters')->findOneBy(array('ip' => $clientIp));
        if ($filters) {
            $filtersArray = array($subcategoryId => $attrbutesFilters);
            $filters->setFiltersJson(serialize($filtersArray));
        } else {
            $filters = new SavedFilters();
            $filters->setIp($clientIp);
            $filtersArray = array($subcategoryId => $attrbutesFilters);
            $filters->setFiltersJson(serialize($filtersArray));
        }
        $entityManager->persist($filters);
        $entityManager->flush();
    }

    /**
     * @param $request
     * @param $per_page
     * @param $order
     * @return array|mixed
     */
    private function setGoodsOrderInCookie($request, $per_page, $order, $category_id)
    {
        $em = $this->getDoctrine()->getManager();
        $response = new Response();
        $userIp = $this->container->get('request')->getClientIp();
        $cookieFilters = $request->cookies->get('order');
        $session = md5(time().rand(0,999));

        if(!empty($cookieFilters)){
            $cookies = unserialize($cookieFilters);
            $session = $cookies['session'];

            if($per_page != NULL){
                $cookies['per_page'] = $per_page;
            }else{
                $cookies['per_page'] = self::PER_PAGE;
            }

            if($order != NULL){
                $cookies['order'] = $order;
            }else{
                $cookies['order'] = self::ORDER;
            }

        }else{
            $cookies = array(
                'session'   => $session,
                'order' => self::ORDER,
                'per_page' => self::PER_PAGE
            );
        }

        $findUserConfig = $em->getRepository('CatalogBackendBundle:UserConfig')
            ->findOneByIpOrSession($category_id, $userIp, $session);

        if(is_null($findUserConfig))
        {
            $userConfig = new UserConfig();
            $userConfig->setSession($session);
            $userConfig->setIp($userIp);
            $userConfig->setCategory($em->getReference('CatalogBackendBundle:Category', $category_id));
            $em->persist($userConfig);
            $em->flush();
        }


        $response->headers->setCookie(new Cookie('order', serialize($cookies)));
        $response->sendHeaders();

        return $cookies;
    }


    /**
     * @param Request $request
     * @param $subcategoryId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetFiltersAction(Request $request, $subcategoryId)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $clientIp = $request->getClientIp();
        $filters = $entityManager->getRepository('CatalogBackendBundle:SavedFilters')->findOneBy(array('ip' => $clientIp));
        if ($filters) {

            $jsonFilters = unserialize($filters->getFiltersJson());
            if (isset($jsonFilters[$subcategoryId])) {

                unset($jsonFilters[$subcategoryId]);
                $filters->setFiltersJson(serialize($jsonFilters));
                $entityManager->persist($filters);
                $entityManager->flush();
            }
        }

        return $this->redirect($this->generateUrl('catalog_goods', array('subcategoryId' => $subcategoryId)));
    }

    /**
     * Remove brand from search filter
     * @param Request $request
     */
    public function ajaxRemoveBrandAction(Request $request)
    {
        $ip = $request->getClientIp();
        $cookies = $request->cookies->get('order');
        if($request->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();

            $brand = $request->request->get('brand');
            $category = $request->request->get('category');

            $userConfig = $em->getRepository('CatalogBackendBundle:UserConfig')
                ->findOneByIpOrSession($category, $ip, $cookies['session']);
            if (!empty($userConfig)) {
                $allBrands = $userConfig->getBrand();

                $searchResult = array_search($brand, $allBrands);

                if (!empty($searchResult)) {
                    unset($allBrands[$searchResult]);
                    $userConfig->setBrand($allBrands);
                    $em->persist($userConfig);
                    $em->flush();
                    $status = 200;
                }else{
                    $status = 400;
                }

            }else{
                $status = 404;
            }

            return new JsonResponse(array(), $status);
        }
    }


    /**
     * @param Request $request
     * @param SubCategory $subcategory
     * @return bool
     */
    public function ajaxRemoveFiltersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->request->get('session');
        $subcategory = $request->request->get('category');
        $remove = $em->getRepository('CatalogBackendBundle:UserConfig')
            ->findOneBy(array('category' => $subcategory, 'session' => $session));
        if($remove !== null)
        {
            $em->remove($remove);
            $em->flush();
            $status = 200;
            $response = array("deleted" => true);
        }else{
            $status = 400;
            $response = array("deleted" => false);
        }

        return new JsonResponse($response, $status);

    }
}