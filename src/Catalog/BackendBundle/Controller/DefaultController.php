<?php

namespace Catalog\BackendBundle\Controller;

use Catalog\BackendBundle\Entity\CategoryAttribute;
use Catalog\BackendBundle\Entity\GoodsAttribute;
use Doctrine\Common\Util\Debug;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function indexAction($name)
    {
        return $this->render('CatalogBackendBundle:Default:index.html.twig', array('name' => $name));
    }

    public function testAction()
    {
        $array = array(
            "SL", "5W-30"
        );

        $arrayNames = array(
            "Спецификации API", "Вязкость"
        );



        /**
         * values + names final array
         */
        $userSettings = array_combine($arrayNames, $array);
        $numberOfSettings = count($userSettings);

        echo '<pre>';
        print_r($userSettings);
        echo '</pre>';


        $testArray = array(
            array(
                "Спецификация ACEA" => "SL"
            ),
            array(
                "Вязкость" => "5W-30"
            )
        );

        echo '<pre>';
        print_r($testArray);
        echo '</pre>';


        $counter = 0;
        foreach($testArray as $arr)
        {
           if(count(array_intersect($userSettings, $arr))>0){
               $counter += 1;
           }
        }
        echo $counter;






        $arrayDefault = count($array);

        $all = $this->getDoctrine()->getManager()->getRepository('CatalogBackendBundle:Goods')
            ->findByAttribute($array, 86);

        echo "<h1>Было извлечено ".count($all)."</h1>";
        $result = array();
        $md5Array = array();
        foreach($all as $item){
            $arrayOfAttributes = array();

            foreach($item->getGoods()->getAttribute() as $attribute){

                $arrayOfAttributes[] = array($attribute->getCategoryAttribute()->getName() => $attribute->getValue());

            }

            $counter = 0;

            foreach($arrayOfAttributes as $settings)
            {
                    $counter += count(array_intersect($userSettings, $settings));
            }


            if($counter >= $numberOfSettings)
            {
                $md5name = md5($item->getGoods()->getArticle());

                if(!in_array($md5name, $md5Array)){
                    $md5Array[] = $md5name;
                    $result[] = $item->getGoods();
                }
            }

//            $intersect = array_intersect($array, $arrayOfAttributes);
//
//            if(count($intersect) >= $arrayDefault ){
//                echo 'yuppi';
//                $md5name = md5($item->getGoods()->getArticle());
//
//                    if(!in_array($md5name, $md5Array)){
//                        $md5Array[] = $md5name;
//                        $result[] = $item;
//                    }
//
//            }

        }

        echo $resultCounter = 0;
        foreach($result as $itemT){

            echo '<hr/><b>'.$itemT->getFullName().' / '. $itemT->getArticle().' - <b>'.count($itemT->getAttribute()).'</b></b><br/>';

            foreach($itemT->getAttribute() as $attribute){
                echo "<br/>".$attribute->getCategoryAttribute()->getName().
                    " / ".$attribute->getValue();


            }
            $resultCounter += 1;
        }

        echo "<h1>Было выведено".$resultCounter;
        echo "</h1>";
//        echo '<pre>';
//        echo Debug::dump($all);
//        echo '</pre>';


        exit;
    }
}
